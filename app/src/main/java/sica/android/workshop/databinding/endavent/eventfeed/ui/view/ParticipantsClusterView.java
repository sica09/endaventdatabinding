package sica.android.workshop.databinding.endavent.eventfeed.ui.view;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sica.android.workshop.databinding.endavent.BR;
import sica.android.workshop.databinding.endavent.R;
import sica.android.workshop.databinding.endavent.common.model.EmployeeItem;

/**
 * Custom view for showing participants in Endavent feed recycler view.
 *
 * @author Marius M.
 */
public class ParticipantsClusterView extends ParticipantsAbstractView {

    /**
     * Used to determine if we should update the participants
     */
    public int mParticipantsSize = 0;

    public ParticipantsClusterView(Context context) {
        this(context, null);
    }

    public ParticipantsClusterView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ParticipantsClusterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(attrs);
    }

    /**
     * Initializes the view.
     *
     * @param attrs
     */
    private void initView(AttributeSet attrs) {
        // Initialize view
    }

    /**
     * Adds a view for each participant for the event. Displays a maximum of 5 small profiles images
     * and then just adds a number for the remaining participants.
     *
     * @param participants List of {@link EmployeeItem} linked to the event.
     */
    @BindingAdapter("participants")
    public static void setParticipants(ParticipantsClusterView clusterView, List<EmployeeItem> participants) {
        if (participants.size() != clusterView.mParticipantsSize) {
            // Always clear before a new set
            clusterView.removeAllViews();
            int numberOfAddedParticipants = 0;

            for (EmployeeItem employeeItem : participants) {
                if (numberOfAddedParticipants > 4) {
                    break;
                }
                final ViewDataBinding viewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(clusterView.getContext()), R.layout
                        .layout_cluster_profile_pic_item, clusterView, false);
                viewDataBinding.setVariable(BR.employee, employeeItem);
                clusterView.addView(viewDataBinding.getRoot());
                numberOfAddedParticipants++;
            }

            // If there is no more room to cluster profile pics add a info text with the remaining participants.
            final int participantsSize = participants.size();
            if (numberOfAddedParticipants < participantsSize) {
                final TextView remainingParticipants = new TextView(clusterView.getContext());
                LayoutParams textViewParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                textViewParams.gravity = Gravity.CENTER_VERTICAL;
                textViewParams.leftMargin = clusterView.getResources().getDimensionPixelOffset(R.dimen.small_margin);
                remainingParticipants.setLayoutParams(textViewParams);
                remainingParticipants.setText("+" + (participantsSize - numberOfAddedParticipants));
                clusterView.addView(remainingParticipants, textViewParams);
            }
            clusterView.mParticipantsSize = participants.size();
        }
    }

}
