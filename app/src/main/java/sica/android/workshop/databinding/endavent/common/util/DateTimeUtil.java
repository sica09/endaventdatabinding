package sica.android.workshop.databinding.endavent.common.util;

import android.databinding.BindingAdapter;
import android.util.Log;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * ImageUtils class to take care of date and time operations and conversions.
 * @author Marius M.
 */
public class DateTimeUtil {

    public static final String STANDARD_DATE_FORMAT = "dd-MM-yyyy'T'HH:mm:ss";
    public static final String STANDARD_DATE_READEABLE_FORMAT = " dd/MM/yyyy  HH:mm";

    /**
     * Converts the string into date as long as the date format is {@value STANDARD_DATE_FORMAT} <br/>
     * e.g 27-10-2016T09:27:37Z
     * @param dateString - the date passed as string under the format above.
     * @return The created {@link Date} object from string date. <br/>
     *         null - if the dateString format is not matching
     */
    public static Date getDateFromString(final String dateString) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(STANDARD_DATE_FORMAT);
        try {
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            Log.e(DateTimeUtil.class.getSimpleName(), "Could not parse string as date. Please check the format.");
            return null;
        }
    }

    /**
     * Returns the given date as text.
     * @param date Date to be displayed as text.
     * @return The date text formated by {@value STANDARD_DATE_FORMAT}
     */
    public static String getDateAsText(final Date date) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(STANDARD_DATE_READEABLE_FORMAT);
        return dateFormat.format(date);
    }
}
