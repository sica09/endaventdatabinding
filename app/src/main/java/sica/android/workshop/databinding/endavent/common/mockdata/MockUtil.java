package sica.android.workshop.databinding.endavent.common.mockdata;

import android.databinding.ObservableArrayList;

import java.util.ArrayList;
import java.util.List;

import sica.android.workshop.databinding.endavent.common.model.EmployeeGrade;
import sica.android.workshop.databinding.endavent.common.model.EmployeeItem;
import sica.android.workshop.databinding.endavent.common.util.DateTimeUtil;
import sica.android.workshop.databinding.endavent.eventfeed.model.EndaventItem;

/**
 * Because this is a POC project there is no real data comming from any cloud, so this data must be
 * simulated manually. This class objective is to do that.
 * @author Marius M.
 */
public class MockUtil {

    /**
     * The mocked employees
     */
    public static final List<EmployeeItem> EMPLOYEES_MOCKED = getEmplyeesMocked();

    /**
     * The mocked Endavents.
     */
    public static final List<EndaventItem> ENDAVENTS_MOCKED = getEventsMocked(EMPLOYEES_MOCKED);

    /**
     * The current user. You!
     */
    public static final EmployeeItem CHUCK_NORRIS = new EmployeeItem();
    static {
        CHUCK_NORRIS.name = "Chuck Norris";
        CHUCK_NORRIS.profileImageUrl = "https://usahitman.com/wp-content/uploads/2012/06/Chuck-norris1.jpg";
        CHUCK_NORRIS.employeeGrade = EmployeeGrade.CHUCK_NORRIS;
        CHUCK_NORRIS.project = "Pe toate proiectele";
        CHUCK_NORRIS.experience = 0;
    }


    private static List<EmployeeItem> getEmplyeesMocked() {
        final List<EmployeeItem> employeeItemsMocked = new ArrayList<>();

        final EmployeeItem ecaterina = new EmployeeItem();
        ecaterina.name = "Aglaia";
        ecaterina.profileImageUrl = "https://fortunedotcom.files.wordpress.com/2015/09/fnw-09-15-15-food_and_drink-12.jpg?quality=80&w=820&h=570&crop=1";
        ecaterina.employeeGrade = EmployeeGrade.MIDDLE;
        ecaterina.project = "Pi banca";
        ecaterina.experience = 1.7d;
        employeeItemsMocked.add(ecaterina);

        final EmployeeItem andromeda = new EmployeeItem();
        andromeda.name = "Andromeda";
        andromeda.profileImageUrl = "http://2.bp.blogspot.com/-Z0BA_oMQ6QI/Ug3D8S3XFgI/AAAAAAABwoI/ueaRnd7LGSo/s1600/Lana+Asanin.jpg";
        andromeda.employeeGrade = EmployeeGrade.JUNIOR;
        andromeda.project = "Ridic pixuri";
        andromeda.experience = 0.2d;
        employeeItemsMocked.add(andromeda);

        final EmployeeItem angeni = new EmployeeItem();
        angeni.name = "Angeni";
        angeni.profileImageUrl = "http://www.cbc.ca/connects/content/images/tricia-helfer-viondra-ascension.gif";
        angeni.employeeGrade = EmployeeGrade.SENIOR;
        angeni.project = "PDR";
        angeni.experience = 9.2d;
        employeeItemsMocked.add(angeni);

        final EmployeeItem betty = new EmployeeItem();
        betty.name = "Betty";
        betty.profileImageUrl = "http://glamourmedispa.com/wp-content/uploads/2014/05/Women-Profile-Picture1.jpg";
        betty.employeeGrade = EmployeeGrade.SENIOR;
        betty.project = "Portofelu`Meu";
        betty.experience = 4d;
        employeeItemsMocked.add(betty);

        final EmployeeItem jimena = new EmployeeItem();
        jimena.name = "Jimena";
        jimena.profileImageUrl = "https://www.czech-single-women.com/foto/eu/inzeraty_zeny/detail/01e3cc7cb103da1891942d756fac734f.jpg?iid=23230&fid=1&t=en";
        jimena.employeeGrade = EmployeeGrade.MIDDLE;
        jimena.project = "Portofelu`Meu";
        jimena.experience = 2d;
        employeeItemsMocked.add(jimena);

        final EmployeeItem esmeralda = new EmployeeItem();
        esmeralda.name = "Esmeralda";
        esmeralda.profileImageUrl = "http://fashionnewest.com/wp-content/uploads/2016/03/cloo401.jpg";
        esmeralda.employeeGrade = EmployeeGrade.JUNIOR;
        esmeralda.project = "Portofelu`Meu";
        esmeralda.experience = 0.4d;
        employeeItemsMocked.add(esmeralda);

        final EmployeeItem fergie = new EmployeeItem();
        fergie.name = "Fergie";
        fergie.profileImageUrl = "http://www.pureapnea.com/images/records/VendulaStrachotovaSTA.jpg";
        fergie.employeeGrade = EmployeeGrade.MIDDLE;
        fergie.project = "DesteaptaCasa";
        fergie.experience = 3d;
        employeeItemsMocked.add(fergie);

        final EmployeeItem vendula = new EmployeeItem();
        vendula.name = "Vendula";
        vendula.profileImageUrl = "https://www.czech-single-women.com/foto/eu/inzeraty_zeny/detail/49611afa7cfa38d2e567efdbb6399b63.jpg?iid=24254&fid=1&t=en";
        vendula.employeeGrade = EmployeeGrade.SENIOR;
        vendula.project = "MniezEmail";
        vendula.experience = 7.3d;
        employeeItemsMocked.add(vendula);

        final EmployeeItem alesandra = new EmployeeItem();
        alesandra.name = "Alesandra";
        alesandra.profileImageUrl = "http://img.tedsa.com/alsandra.jpg";
        alesandra.employeeGrade = EmployeeGrade.JUNIOR;
        alesandra.project = "Portofelu'Meu";
        alesandra.experience = 7.3d;
        employeeItemsMocked.add(alesandra);

        final EmployeeItem fernanda = new EmployeeItem();
        fernanda.name = "Fernanda";
        fernanda.profileImageUrl = "http://top10photos.com/wp-content/uploads/2015/02/Top-10-Cities-with-the-Most-Beautiful-Women-9.jpg";
        fernanda.employeeGrade = EmployeeGrade.MIDDLE;
        fernanda.project = "Portofelu'Meu";
        fernanda.experience = 3.2d;
        employeeItemsMocked.add(fernanda);

        final EmployeeItem anica = new EmployeeItem();
        anica.name = "Anica";
        anica.profileImageUrl = "https://www.czech-single-women.com/foto/eu/inzeraty_zeny/detail/ce08eaf47673a9748dca3ba9dd2c912f.jpg?iid=18732&fid=1&t=en";
        anica.employeeGrade = EmployeeGrade.JUNIOR;
        anica.project = "MniezEmail";
        anica.experience = 3.2d;
        employeeItemsMocked.add(anica);

        final EmployeeItem felicia = new EmployeeItem();
        felicia.name = "Felicia";
        felicia.profileImageUrl = "http://www.exboyfriendrecovery.com/wp-content/uploads/2013/07/good-profile-picture.jpg";
        felicia.employeeGrade = EmployeeGrade.JUNIOR;
        felicia.project = "DesteaptaCasa";
        felicia.experience = 5.6d;
        employeeItemsMocked.add(felicia);

        final EmployeeItem chelsea = new EmployeeItem();
        chelsea.name = "Chelsea";
        chelsea.profileImageUrl = "http://dailydater.com/wp-content/uploads/2015/01/One-Sneaky-Way-To-Make-Women-Adore-You-in-Your-Online-Dating-Profile.jpg";
        chelsea.employeeGrade = EmployeeGrade.SENIOR;
        chelsea.project = "Asistenta lu' Marius";
        chelsea.experience = 0.7d;
        employeeItemsMocked.add(chelsea);

        return employeeItemsMocked;
    }

    private static List<EndaventItem> getEventsMocked(final List<EmployeeItem> employeeItems) {
        final ArrayList<EndaventItem> endaventsMocked = new ArrayList<>();

        // Summer Party !!!
        final EndaventItem summerParty = new EndaventItem();
        summerParty.imageUrl = "https://i.ytimg.com/vi/RQpyK87BB7w/maxresdefault.jpg";
        summerParty.title = "Summer party";
        summerParty.shortDescription = "Ase party n-ai vazut. Martinica Scorsese ala incearca sa tot obtina o invitatie sa filmeze al 2-lea film de oscar.";
        summerParty.description = "Deci cum am zis, e pe barosaneala treaba. Anu asta il aducem pe Snoop-Dog, ca cica sa le faca in ciuda la aia de la Untold. Da daca vine, noi nu ne suparam fratica.";
        summerParty.startDateTime = DateTimeUtil.getDateFromString("15-08-2016T20:00:00");
        summerParty.endDateTime = DateTimeUtil.getDateFromString("16-08-2016T12:00:00");
        summerParty.listOfParticipants = new ObservableArrayList<>();
        summerParty.listOfParticipants.addAll(new ArrayList<>(employeeItems));
        endaventsMocked.add(summerParty);

        // Innovation lab !!!
        final EndaventItem innovationLab = new EndaventItem();
        innovationLab.imageUrl = "https://scontent-cdg2-1.xx.fbcdn.net/hphotos-xfp1/t31.0-8/12771591_10207764296279663_4493060068616910926_o.jpg";
        innovationLab.title = "Innovation lab";
        innovationLab.shortDescription = "Proiectele NASA de aici pornesc. De fapt innovation labs e NASA sub alta denumire.";
        innovationLab.description = "Eight teams from several Endava locations got together for a full weekend of innovation. We are excited by all the brilliant ideas that came out from this edition.";
        innovationLab.startDateTime = DateTimeUtil.getDateFromString("03-07-2016T16:00:00");
        innovationLab.endDateTime = DateTimeUtil.getDateFromString("05-07-2016T14:00:00");
        innovationLab.listOfParticipants = new ObservableArrayList<>();
        innovationLab.listOfParticipants.addAll(new ArrayList<>(employeeItems.subList(5,10)));
        endaventsMocked.add(innovationLab);

        // Kick-off !!!
        final EndaventItem kickOff = new EndaventItem();
        kickOff.imageUrl = "https://i.ytimg.com/vi/NAS7hcfG4QU/maxresdefault.jpg";
        kickOff.title = "Endava Kick-off";
        kickOff.shortDescription = "Doresti sa aflii cine era in firma acum 15 ani pe cand tu inca te jucai cu cubulete? Hai la kick-off si ai sa aflii";
        kickOff.description = "Un eveniment unde toata lumea se lauda cu munca altora. E interzis sa te lauzi cu munca ta (nu primesti mancare daca faci asta). Apropo de mancare, e sofisticata frate, deci hai imbracat la ceva ce poti pune cravata. Crevetele nu merge cu tricou in ...";
        kickOff.startDateTime = DateTimeUtil.getDateFromString("28-09-2016T18:00:00");
        kickOff.endDateTime = DateTimeUtil.getDateFromString("28-09-2016T18:15:00");
        kickOff.listOfParticipants = new ObservableArrayList<>();
        kickOff.listOfParticipants.addAll(new ArrayList<>(employeeItems.subList(1,8)));
        endaventsMocked.add(kickOff);

        // Endava Open !!!
        final EndaventItem endavaOpen = new EndaventItem();
        endavaOpen.imageUrl = "http://s3.india.com/wp-content/uploads/2015/09/Simona-Halep-US-Open2015.jpg";
        endavaOpen.title = "Endava Open";
        endavaOpen.shortDescription = "Cum nu a mai castigat nici un turneu in ultima vreme, Halep isi incearca norocu la Endava Open.";
        endavaOpen.description = "Inscrie-te acum la turneul de mare salam. Primii 2 concurenti inscrisi vor primi si putea bea apa plata Dorna. Restu, doar apa de la Watercooler-ele endava";
        endavaOpen.startDateTime = DateTimeUtil.getDateFromString("21-08-2016T12:00:00");
        endavaOpen.endDateTime = DateTimeUtil.getDateFromString("23-08-2016T17:15:00");
        endavaOpen.listOfParticipants = new ObservableArrayList<>();
        endavaOpen.listOfParticipants.addAll(new ArrayList<>(employeeItems.subList(7,12)));
        endaventsMocked.add(endavaOpen);


        // Endava Open Day !!!
        final EndaventItem endavaOpenDay = new EndaventItem();
        endavaOpenDay.imageUrl = "http://evenimenteit.ro/wp-content/uploads/2016/02/EndavaOpenDayCluj1454958276.jpg";
        endavaOpenDay.title = "Endava Open Day";
        endavaOpenDay.shortDescription = "Endava is organising the Endava Open Day, a meeting with those who wish to start their careers or to make the next step alongside the Endava team.";
        endavaOpenDay.description = "The Open Day event targets passionate and enthusiastic students & graduates interested to follow a career in Software Development, Software Testing, Managed Services (Service Desk Analyst) and Shared Services (Screening & HR Admin). \n" +
                "\n" +
                "During the presentation sessions, the participants have the opportunity to meet the Endava team, to experience the working environment, to see how the Endava teams work and to interact with team leaders and previous generations of graduates & interns.";
        endavaOpenDay.startDateTime = DateTimeUtil.getDateFromString("21-04-2016T10:00:00");
        endavaOpenDay.endDateTime = DateTimeUtil.getDateFromString("23-04-2016T16:30:00");
        endavaOpenDay.listOfParticipants = new ObservableArrayList<>();
        endavaOpenDay.listOfParticipants.addAll(new ArrayList<>(employeeItems.subList(3,11)));
        endaventsMocked.add(endavaOpenDay);

        return endaventsMocked;
    }
}
