package sica.android.workshop.databinding.endavent.common.model;

import java.io.Serializable;

/**
 * User object containing employee data.
 * @author Marius M.
 */
public class EmployeeItem implements Serializable {

    /**
     * Name of the employee.
     */
    public String name;

    /**
     * Employee profile picture url.
     */
    public String profileImageUrl;

    /**
     * Employee grade (e.g junior, senior, etc).
     */
    public EmployeeGrade employeeGrade;

    /**
     * The name of the project the employee is currently on.
     */
    public String project;

    /**
     * Experience of work in years
     */
    public double experience;

}
