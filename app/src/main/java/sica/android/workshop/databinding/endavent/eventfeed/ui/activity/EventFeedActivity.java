package sica.android.workshop.databinding.endavent.eventfeed.ui.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import sica.android.workshop.databinding.endavent.R;
import sica.android.workshop.databinding.endavent.common.mockdata.MockUtil;
import sica.android.workshop.databinding.endavent.databinding.ActivityEventFeedBinding;
import sica.android.workshop.databinding.endavent.eventfeed.model.EndaventItem;
import sica.android.workshop.databinding.endavent.eventfeed.ui.adapter.EndaventRecyclerAdapter;

/**
 * Activity which displayes the Endavent feed.
 * @author Marius M
 */
public class EventFeedActivity extends AppCompatActivity {

    /**
     * The event feed recycler view.
     */
    private ActivityEventFeedBinding mBinding;

    /**
     * The recycler adapter for the recycler view.
     */
    private EndaventRecyclerAdapter mEndaventRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_event_feed);
        setSupportActionBar(mBinding.toolbar);

        mBinding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mBinding.contentFeed.eventFeedRecyclerView.setLayoutManager(layoutManager);
        mEndaventRecyclerAdapter = new EndaventRecyclerAdapter(MockUtil.ENDAVENTS_MOCKED);
        mBinding.contentFeed.eventFeedRecyclerView.setAdapter(mEndaventRecyclerAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event_feed, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_simulate_sync) {
            simulateSync();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Method which simulates short description update for the first and ast item in the list.
     */
    private void simulateSync() {
        final EndaventItem firstItem = MockUtil.ENDAVENTS_MOCKED.get(0);
        firstItem.setShortDescription("UPDATED!!! " + firstItem.getShortDescription());
        final EndaventItem lastItem = MockUtil.ENDAVENTS_MOCKED.get(MockUtil.ENDAVENTS_MOCKED.size() - 1);
        lastItem.setShortDescription("UPDATED LAST !!! " + lastItem.getShortDescription());
    }
}
