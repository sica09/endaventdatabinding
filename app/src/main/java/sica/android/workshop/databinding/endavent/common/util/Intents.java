package sica.android.workshop.databinding.endavent.common.util;

/**
 * Class responsible for helding intent data such as extras keys, bundles, etc.
 * @author Marius M.
 */
public class Intents {

    /**
     * Extras key to be used to send endavent object within activities.
     */
    public static final String EXTRA_ENDAVENT_ITEM = "extra_endavent_item";
}
