package sica.android.workshop.databinding.endavent.eventfeed.ui.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import sica.android.workshop.databinding.endavent.BR;
import sica.android.workshop.databinding.endavent.R;
import sica.android.workshop.databinding.endavent.common.mockdata.MockUtil;
import sica.android.workshop.databinding.endavent.common.util.NavigationUtil;
import sica.android.workshop.databinding.endavent.eventfeed.model.EndaventItem;

/**
 * The recycler view adapter containing display logic of the event feed.
 * @author Marius M.
 */
public class EndaventRecyclerAdapter extends RecyclerView.Adapter<EndaventRecyclerAdapter.EndaventViewHolder>{

    /**
     * The list of endavent items which will fill the adapter.
     */
    private List<EndaventItem> mEndaventItems;

    /**
     * Constructor which takes a list of events to be displayed.
     * @param endavents
     */
    public EndaventRecyclerAdapter(final List<EndaventItem> endavents) {
        mEndaventItems = endavents;
    }

    @Override
    public EndaventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_endavent_item, parent, false);

        return new EndaventViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(EndaventViewHolder holder, int position) {
        holder.setBindingData(getItem(position));
    }

    @Override
    public int getItemCount() {
        return mEndaventItems.size();
    }

    /**
     * Returns the event on the requested position.
     * @param position - position in the adapter
     * @return {@link EndaventItem} at the requested position.
     */
    private EndaventItem getItem(final int position) {
        return mEndaventItems.get(position);
    }

    /**
     * The view holder of the adapter views.
     */
    public final class EndaventViewHolder extends RecyclerView.ViewHolder {

        /**
         * The view data binding holding all the views of the holder.
         */
        private ViewDataBinding mViewDataBinding;

        /**
         * The {@link EndaventItem} used by this view holder.
         */
        private EndaventItem mEndaventItem;

        /**
         * Constructor for view holder which has the inflated view as parameter.
         * @param itemView The inflated view.
         */
        public EndaventViewHolder(View itemView) {
            super(itemView);
            mViewDataBinding = DataBindingUtil.bind(itemView);
        }

        /**
         * Binds the data to the view holder.
         * @param endaventItem The event item to be binded with the view holder using data binding.
         */
        public void setBindingData(final EndaventItem endaventItem) {
            mEndaventItem = endaventItem;
            mViewDataBinding.setVariable(BR.endavent, endaventItem);
            mViewDataBinding.setVariable(BR.viewHolder, this);
            mViewDataBinding.executePendingBindings();
        }

        /**
         * Callback for event clicks.
         * @param view
         */
        @SuppressWarnings("unused")
        public void onEndaventClicked(@NonNull final View view) {
            if (view.getId() == R.id.endavent_attend) {
                // Attend button pressed
                if (!mEndaventItem.listOfParticipants.get(0).name.equals("Chuck Norris")) {
                    // If it doesn't exist
                    mEndaventItem.listOfParticipants.add(0, MockUtil.CHUCK_NORRIS);
                }
            } else {
                // starts the detail activity
                NavigationUtil.startEventDetailActivity(view.getContext(), mEndaventItem);
            }
        }
    }
}
