package sica.android.workshop.databinding.endavent.eventfeed.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableArrayList;

import java.io.Serializable;
import java.util.Date;

import sica.android.workshop.databinding.endavent.BR;
import sica.android.workshop.databinding.endavent.common.model.EmployeeItem;

/**
 * Item containing the Endava event details.
 * @author Marius M.
 */
public class EndaventItem extends BaseObservable implements Serializable {

    /**
     * The event image url.
     */
    public String imageUrl;

    /**
     * The event title.
     */
    public String title;

    /**
     * The evet description.
     */
    public String description;

    /**
     * The event short description/abstract (displayed in the list)
     */
    public String shortDescription;

    /**
     * The list of participants.
     */
    public ObservableArrayList<EmployeeItem> listOfParticipants;

    /**
     * The date and time when the event starts.
     */
    public Date startDateTime;

    /**
     * The date and time when the event ends.
     */
    public Date endDateTime;

    @Bindable
    public String getShortDescription() {
        return shortDescription;
    }

    /**
     * Sets the short description and updates the listening views with the new value.
     * Another way to use this is by having an ObserverField<T> mShortDescription and that does handling for you without having to define
     * getters and setters. Also you wouldn't need to extend BaseObservable as well.
     * @param shortDescription
     */
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
        notifyPropertyChanged(BR.shortDescription);
    }
}
