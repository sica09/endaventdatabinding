package sica.android.workshop.databinding.endavent.common.util;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import sica.android.workshop.databinding.endavent.R;

/**
 * Contains utility methods to manage images.
 * @author Marius M.
 */
@SuppressWarnings("unused")
public class ImageUtils {

    /**
     * Loads image from url using Picasso library
     * @param imageView The imageview to load the image into
     * @param imageUrl The image url
     * @param errorPlaceholder The drawable resource to be displayed in case of image laoding fails
     */
    @BindingAdapter(value = {"imageUrl", "placeholder"}, requireAll = false)
    public static void loadImageFromUrl(ImageView imageView, String imageUrl, Drawable errorPlaceholder) {
        final RequestCreator requestCreator = Picasso.with(imageView.getContext()).load(imageUrl);
        if (errorPlaceholder != null) {
            requestCreator.error(errorPlaceholder);
        }
        requestCreator.into(imageView);
    }
}
