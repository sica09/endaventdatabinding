package sica.android.workshop.databinding.endavent.eventfeed.ui.view;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.github.siyamed.shapeimageview.CircularImageView;

import sica.android.workshop.databinding.endavent.R;
import sica.android.workshop.databinding.endavent.common.model.EmployeeItem;

/**
 * Base class for participants view.
 */
public class ParticipantsAbstractView extends LinearLayout {

    public ParticipantsAbstractView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * Sets the shader color of the profile pic based on his profesional grade.
     * @param imageView the profile pic
     * @param participant the employee
     */
    @BindingAdapter("shaderColor")
    public static void setShaderColor(final CircularImageView imageView, final EmployeeItem participant) {
        final int shaderColor;
        final Context context = imageView.getContext();
        switch (participant.employeeGrade) {
            case JUNIOR:
                shaderColor = ContextCompat.getColor(context, R.color.junior);
                break;
            case MIDDLE:
                shaderColor = ContextCompat.getColor(context, R.color.middle);
                break;
            case SENIOR:
                shaderColor = ContextCompat.getColor(context, R.color.senior);
                break;
            case CHUCK_NORRIS:
                shaderColor = ContextCompat.getColor(context, R.color.chuck_norris);
                break;
            default:
                shaderColor = ContextCompat.getColor(context, R.color.divider);
        }
        imageView.setBorderColor(shaderColor);
    }
}
