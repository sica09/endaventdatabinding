package sica.android.workshop.databinding.endavent.common.model;

import java.io.Serializable;

/**
 * Contains the employee grade in the company (e.g Junior Developer, Middle Developer, Senior Developer)
 * @author Marius M.
 */
public enum EmployeeGrade implements Serializable {
    JUNIOR("Junior Developer"),
    MIDDLE("Middle Developer"),
    SENIOR("Senior Developer"),
    CHUCK_NORRIS("Chuck Norris");

    /**
     * The grade name.
     */
    private String mGradeName;

    /**
     * Enum constructor with grad name.
     * @param gradeName
     */
    EmployeeGrade(final String gradeName) {
        mGradeName = gradeName;
    }

    @Override
    public String toString() {
        return mGradeName;
    }
}
