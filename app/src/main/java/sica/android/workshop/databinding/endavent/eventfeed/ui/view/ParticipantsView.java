package sica.android.workshop.databinding.endavent.eventfeed.ui.view;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import java.util.List;

import sica.android.workshop.databinding.endavent.R;
import sica.android.workshop.databinding.endavent.common.model.EmployeeItem;
import sica.android.workshop.databinding.endavent.databinding.LayoutParticipantItemBinding;

/**
 * Displayes the list of participants.
 *
 * @author Marius M.
 */
public class ParticipantsView extends ParticipantsAbstractView {

    public ParticipantsView(Context context) {
        this(context, null);
    }

    public ParticipantsView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ParticipantsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(attrs);
    }

    /**
     * Initializes the view.
     *
     * @param attrs
     */
    private void initView(AttributeSet attrs) {
        // Initialize view
    }

    /**
     * Sets the participants data to this view.
     *
     * @param participants
     */
    @BindingAdapter("participants")
    public static void setParticipants(ParticipantsView participantsView, List<EmployeeItem> participants) {
        participantsView.removeAllViews();
        for (EmployeeItem participant : participants) {
            final LayoutParticipantItemBinding participantView = DataBindingUtil.inflate(
                    LayoutInflater.from(participantsView.getContext()), R.layout.layout_participant_item, participantsView, false);
            participantView.setParticipant(participant);
            participantsView.addView(participantView.getRoot());
        }
    }
}
