package sica.android.workshop.databinding.endavent.eventfeed.ui.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import sica.android.workshop.databinding.endavent.R;
import sica.android.workshop.databinding.endavent.common.mockdata.MockUtil;
import sica.android.workshop.databinding.endavent.common.util.Intents;
import sica.android.workshop.databinding.endavent.databinding.ActivityEventDetailBinding;
import sica.android.workshop.databinding.endavent.eventfeed.model.EndaventItem;

/**
 * Activity containing the event details.
 *
 * @author Marius M.
 */
public class EventDetailActivity extends AppCompatActivity {

    /**
     * Binding which holds the views and binded data.
     */
    private ActivityEventDetailBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_event_detail);
        final EndaventItem endaventItem = (EndaventItem) getIntent().getSerializableExtra(Intents.EXTRA_ENDAVENT_ITEM);
        mBinding.setEndavent(endaventItem);
        initToolbar();
        initViews(endaventItem);
    }

    /**
     * Initializaes the toolbar and it's elements.
     */
    private void initToolbar() {
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mBinding.collapsingToolbar.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
        mBinding.collapsingToolbar.setContentScrimColor(ContextCompat.getColor(this, R.color.eColorPrimary));
    }

    /**
     * Initializes the views.
     */
    private void initViews(final EndaventItem endaventItem) {
        mBinding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!endaventItem.listOfParticipants.get(0).name.equals("Chuck Norris")) {
                    // If not attended already
                    endaventItem.listOfParticipants.add(0, MockUtil.CHUCK_NORRIS);
                }
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
