package sica.android.workshop.databinding.endavent.common.util;

import android.content.Context;
import android.content.Intent;

import sica.android.workshop.databinding.endavent.eventfeed.model.EndaventItem;

/**
 * Class responsible with navigation in the application.
 * @author Marius M.
 */
public class NavigationUtil {

    /**
     * The prefix to ensure intent filter uniqueness.
     */
    private static final String APPLICATION_PREFIX = "endavent.";

    /**
     * Action used to start Event detail activity.
     */
    public static final String ACTION_START_EVENT_DETAIL_ACTIVITY = APPLICATION_PREFIX + "START_EVENT_DETAIL_ACTIVITY";

    /**
     * Starts the Endavent Detail Activity which contains event details.
     * @param context Context to start activity from.
     * @param endaventItem The event item containing event details.
     */
    public static void startEventDetailActivity(final Context context, final EndaventItem endaventItem) {
        final Intent intent = new Intent(ACTION_START_EVENT_DETAIL_ACTIVITY);
        intent.putExtra(Intents.EXTRA_ENDAVENT_ITEM, endaventItem);
        context.startActivity(intent);
    }
}
